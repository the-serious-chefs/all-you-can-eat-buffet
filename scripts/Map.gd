extends Navigation2D

# Called when the node enters the scene tree for the first time.
func _ready():
    var map = get_child(0).get_child(0)
    # Set collision layer so dash will collide with walls
    map.set_collision_layer_bit(1,true)
    # Generate A* graph
    get_child(0).remove_child(map)
    add_child(map)
