extends KinematicBody2D

# export var wall_hit_sound: AudioStream
# export var enemy_hit_sound: AudioStream

var speed = 400

func _physics_process(delta):
    var facing = Vector2(sin(rotation), -cos(rotation))
    var collision = move_and_collide(delta * facing * speed, false)
    if collision != null:
        if collision.collider.has_method("damage"):
            collision.collider.damage()
        queue_free()
