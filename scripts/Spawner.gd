extends Node2D

export var enemy: PackedScene
export var wait_time: float = 1.0

export var mob_limit = 16
const enemies = [preload("res://scenes/Sigmund.tscn"), \
                 preload("res://scenes/Octopus.tscn"), \
                 preload("res://scenes/FireWizard.tscn")]

# Maybe this can be used for difficulty?
var spawn_counter = 0

func _ready():
    add_to_group("spawners")
    # $Timer.connect("timeout", self, "spawn_enemy")
    # $Timer.wait_time = wait_time
    if has_node("AnimationPlayer"):
        $AnimationPlayer.play("Spawner")

func choose_enemy():
    var roll = randi() % 100
    # Critical failure = rat
    if roll <= 10:
        return 0
    var modified_roll = roll + spawn_counter/5
    if modified_roll >= 85:
        return 2
    elif modified_roll >= 70:
        return 1
    else:
        return 0

func instance_enemy(index):
    var enemy_instance = enemies[index].instance()
    $Spawned.add_child(enemy_instance)

func instance_named_enemy(name,index):
    var enemy_instance = enemies[index].instance()
    $Spawned.add_child(enemy_instance)

# Choose a random enemy and spawn it
func spawn_random():
    instance_enemy(choose_enemy())

func spawn_enemy():
    var mobs = get_tree().get_nodes_in_group("enemies")
    var non_stunned_mobs = 0
    
    for mob in mobs:
        if(!mob.is_staggered()):
            non_stunned_mobs += 1
    
    # don't spawn too many mobs!
    if(non_stunned_mobs >= mob_limit):
        return
    
    # If we're networked
    if (get_tree().has_network_peer()):
        # Then check if we're the master
        if (is_network_master()):
            # And if so spawn an enemy + sync clients
            var next_name = "Enemy" + str(spawn_counter)
            # Choose an enemy to spawn
            var next_enemy = choose_enemy()
            rpc("_spawn_enemy",next_name,next_enemy)
            spawn_counter += 1

    # Otherwise just spawn an enemy
    else:
        spawn_random()

remotesync func _spawn_enemy(name,index):
    instance_named_enemy(name,index)
