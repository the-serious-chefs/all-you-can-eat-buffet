extends AudioStreamPlayer

func _ready():
    get_tree().connect("node_added", self, "handle_new_node")
    # ridiculous hack
    get_tree().reload_current_scene()

func handle_new_node(node):
    if node is BaseButton:
        node.connect("button_down", self, "play")
