extends Sprite

const textures = [ \
        preload("res://assets/blood1.png"), \
        preload("res://assets/blood3.png"), \
        preload("res://assets/blood4.png"), \
        preload("res://assets/blood5.png"), \
        preload("res://assets/blood6.png"), \
        preload("res://assets/blood7.png"), \
        preload("res://assets/blood8.png")]

func _ready():
    texture = textures[randi() % textures.size()]
    rotation = randf() * 3.14 * 2
