extends Control

# Player info, associate ID to data
var player_info = {}
# Info we send to other players
var my_info = { name = "INVALID NAME", offset = Vector2(0,0) }

func _ready():
    get_tree().connect("network_peer_connected", self, "_player_connected")
    get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
    get_tree().connect("connected_to_server", self, "_connected_ok")
    get_tree().connect("connection_failed", self, "_connected_fail")
    get_tree().connect("server_disconnected", self, "_server_disconnected")

    # Get the player name
    var f = File.new()
    f.open("user://username", File.READ)
    my_info["name"] = f.get_line()
    f.close()

    player_info[1] = my_info

    if not get_tree().is_network_server():
        var button = get_node("root/buttons/start")
        button.visible = false




func _player_connected(id):
    pass


func _player_disconnected(id):
    player_info.erase(id) # Erase player from info.

    # Erase player from world
    if self.has_node("../World"):
        var players = get_node("/root/World/players")
        var child = players.get_node(str(id))
        players.remove_child(child)
        child.queue_free()

func _connected_ok():
    # Only called on clients, not server. Send my ID and info to all the other peers.
    rpc("register_player", get_tree().get_network_unique_id(), my_info)

func _server_disconnected():
    # TODO: actually show error message instead of just booting
    # user back to main screen
    print("SERVER DISCONNECTED")
    shutdown()

remotesync func shutdown():
    # Unload the game and lobby
    Globals.unload_world()

    get_tree().change_scene("res://scenes/Menu.tscn")
    get_tree().set_network_peer(null)

func _connected_fail():
    print("CONNECTION ERROR")
    get_tree().change_scene("res://scenes/Menu.tscn")

remote func register_player(id, info):
    # Store the info
    player_info[id] = info

    # Set the spawn offset of the client
    var index = player_info.keys().find(id) + 1
    var x = 40 * (index % 2)
    var y = 40 * (index / 2)
    rpc_id(id,"set_offset",x,y)
    player_info[id].offset = Vector2(x,y)

    # If I'm the server, let the new guy know about existing players.
    if get_tree().is_network_server():
        # Send my info to new player
        rpc_id(id, "register_player", 1, my_info)
        # Send the info of existing players
        for peer_id in player_info:
            rpc_id(id, "register_player", peer_id, player_info[peer_id])

func _process(delta):

    # Call function to update lobby UI here
    var connected = get_node("root/names/connected")
    connected.text = ""

    # Add the names of all of the clients
    for player in player_info.values():
        connected.text = connected.text + player["name"] + "\n"

    # Check to see if the server hung up
    if get_tree().network_peer.get_connection_status() == 0:
        _server_disconnected()

func _quit():
    # Disconnect
    get_tree().set_network_peer(null)
    # And then get back to the menu
    get_tree().change_scene("res://scenes/Menu.tscn")


func _on_start_pressed():
    # TODO: This only works if the host starts a game
    var temp_world = preload("res://scenes/World.tscn").instance()
    var level = temp_world.get_random_map()
    temp_world.queue_free()
    rpc("pre_configure_game",level)
    #pre_configure_game()


remotesync func pre_configure_game(level):
    # TODO: loading screen
    get_tree().set_pause(true) # Pre-pause

    var selfPeerID = get_tree().get_network_unique_id()

    var which_level = "res://scenes/World.tscn"

    # Load world
    var world = load(which_level).instance()
    # Load the specified map
    world.instance_map(level)
    get_node("/root").add_child(world)


    # Load other players
    for p in player_info:
        var player = preload("res://scenes/Player.tscn").instance()
        player.position += player_info[p].offset
        player.is_multiplayer = true
        player.set_name(str(p))
        player.set_network_master(p) # Will be explained later
        get_node("/root/World/players").add_child(player)

    # Tell server (remember, server is always ID=1) that this peer is done pre-configuring.
    rpc_id(1, "done_preconfiguring", selfPeerID)

remotesync func set_offset(x,y):
    my_info.offset = Vector2(x,y)

var players_done = []

remotesync func done_preconfiguring(who):
    # Here are some checks you can do, for example
    assert(get_tree().is_network_server())
    assert(who in player_info) # Exists
    assert(not who in players_done) # Was not added yet

    players_done.append(who)

    if players_done.size() == player_info.size():
        rpc("post_configure_game")
        #post_configure_game()

remotesync func post_configure_game():
    visible = false
    get_tree().set_pause(false)
