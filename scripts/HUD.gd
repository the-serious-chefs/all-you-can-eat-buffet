extends Control

var score: int = 0

func _ready():
    pass

func set_health(health):
    get_node("TopBar/Bars/HealthBar").value = health

func add_to_score(amt):
    score += amt
    $TopBar.get_node("Score").set_text("Score: " + str(score))

func get_score():
    return score