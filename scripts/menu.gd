extends VBoxContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const SERVER_PORT = 5427
const MAX_PLAYERS = 6

# Called when the node enters the scene tree for the first time.
func _ready():
	var file = File.new()
	if !file.file_exists("user://score"):
		return
	file.open("user://score", File.READ)
	var score = file.get_32()
	$Score.set_text("Last score: " + str(score))
	file.close()
	if !file.file_exists("user://high_score"):
		return
	file.open("user://high_score", File.READ)
	score = file.get_32()
	$HighScore.set_text("High score: " + str(score))
	file.close()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#       pass

func _on_Start_pressed():
        get_tree().change_scene("res://scenes/World.tscn")


func _on_Credits_pressed():
        get_tree().change_scene("res://scenes/Credits.tscn")


func _on_Quit_pressed():
        get_tree().quit()



func _on_Join_pressed():
        var f = File.new()
        f.open("user://connect_to",File.READ)
        var server = f.get_line()
        f.close()
        print(server)
        var peer = NetworkedMultiplayerENet.new()
        peer.create_client(server, SERVER_PORT)
        get_tree().set_network_peer(peer)
        get_tree().change_scene("res://scenes/Lobby.tscn")


func _on_Host_pressed():
        var peer = NetworkedMultiplayerENet.new()
        peer.create_server(SERVER_PORT, MAX_PLAYERS)
        get_tree().set_network_peer(peer)
        get_tree().change_scene("res://scenes/Lobby.tscn")
