extends Node2D

# Keeps track of the movement the player is going to do
# Resets on each frame
var move_x = 0
var move_y = 0

# Spectator speed - gotta go fast
const speed = 500

var controller = false

var is_multiplayer = false

var saved_trans = null

var last_mouse_pos = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
    if not is_multiplayer:
        $Camera.current = true
    elif is_network_master():
        $Camera.current = true

master func handle_rotation():
    # Get a vec2 of the right stick
    var controller_vec2 = Vector2(Input.get_joy_axis(0, JOY_AXIS_2), Input.get_joy_axis(0, JOY_AXIS_3))
    # And the current position of the mouse
    var mouse_pos = get_global_mouse_position()

    # Do some checks to see whether we should stay at the current
    # controller setting

    # If the player moves the mouse then we shouldn't be in controller mode
    if mouse_pos != last_mouse_pos:
        controller = false
    # And if the player gave input on a joystick then we should be in
    # controller mode
    elif controller_vec2.length() != 0:
        controller = true
    # Otherwise we keep the same setting

    # Spin towards mouse
    # Hotly debated: make mouse aiming behave like controller aiming
    # or keep it a point-and-click adventure?
    if not controller:
        # Point the player at the mouse
        rotation = global_position.angle_to_point(mouse_pos) - 3.14/2
    else:
        # Or spin towards controller right axis direction

        # This will be true if there's no changes these axes
        if controller_vec2.length() != 0:
            # Do you enjoy magic numbers?
            rotation = controller_vec2.angle() + 3.14/2
    last_mouse_pos = get_global_mouse_position()

master func get_input():
        move_x = Input.get_action_strength('right') - Input.get_action_strength('left')
        # Up is negative???
        move_y = Input.get_action_strength('down') - Input.get_action_strength('up')

func clear_inputs():
    # Reset inputs
    # This makes it so the player doesn't keep moving
    # after they've released the movement key
    move_x = 0
    move_y = 0

func proj(u, v):
    return u * u.dot(v) / u.dot(u)

func _process(delta):
    if (is_multiplayer && !is_network_master()):
        return

    if is_multiplayer:
        var players = get_node("/root/World/players")
        if players.get_child_count() <= 1:
            # If we're the last player shutdown the lobby
            var lobby = get_node("/root/Lobby")
            lobby.rpc("shutdown")


    clear_inputs() 

    get_input()
    handle_rotation()

    var to_move = Vector2(move_x,move_y)
    if to_move.length_squared() > 1.0:
        # Normalize vector so you can't move faster diagonally
        to_move = to_move.normalized()
    # And finally actually perform the movement
    var linear_velocity = to_move * speed * delta

    position += linear_velocity
