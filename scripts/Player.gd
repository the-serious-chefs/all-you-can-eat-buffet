extends RigidBody2D

var bacon_bits = preload("res://scenes/BaconBits.tscn")

export var eats_stream: AudioStream
export var shoots_stream: AudioStream

# Keeps track of the movement the player is going to do
# Resets on each frame
var move_x = 0
var move_y = 0
var fire_rate_multiplier = 0.0
var can_fire = true
var low_meat = false

onready var normal_layers = collision_layer

export var health_meater = 30

enum State {
    NORMAL,
    DASHING
}
var state = State.NORMAL

const DASH_DISTANCE = 175.0
export var dash_alpha: float = 0.0
var dash_from: Vector2
var dash_to: Vector2
var dash_offset: Vector2

const WALLS = 2

const BASH_RANGE = 200
const BASH_IMPULSE = 2000
const BASH_ANGLE = 45 * (3.14 / 180)

var last_mouse_pos = Vector2(0, 0)

# Player speed - gotta go fast
const speed = 250

var controller = false

var is_multiplayer = false

var saved_trans = null
#var new_position = Vector2(0,0);
#var new_rotation = 0;
#var new_linear_velocity = Vector2(0,0);

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
    contact_monitor = true
    contacts_reported = 1
    $DashAnimation.playback_process_mode = AnimationPlayer.ANIMATION_PROCESS_PHYSICS
    $DashAnimation.connect("animation_finished", self, "stop_dash")
    add_to_group("players")
    if not is_multiplayer:
        $Camera.current = true
    elif is_network_master():
        $Camera.current = true
    update_health()

func _input(event):
    # If the player moves the mouse then we shouldn't be in controller mode
    if event is InputEventMouseMotion:
        controller = false
    # And if the player gave input on a joystick then we should be in
    # controller mode
    elif event is InputEventJoypadMotion:
        if event.axis == JOY_AXIS_2 or event.axis == JOY_AXIS_3:
            controller = true
    # Otherwise we keep the same setting

master func handle_rotation():
    # Get a vec2 of the right stick
    var controller_vec2 = Vector2(Input.get_joy_axis(0, JOY_AXIS_2), Input.get_joy_axis(0, JOY_AXIS_3))
    # And the current position of the mouse
    var mouse_pos = get_global_mouse_position()
    var local_mouse_pos = get_local_mouse_position()

    # Spin towards mouse
    # Hotly debated: make mouse aiming behave like controller aiming
    # or keep it a point-and-click adventure?
    if not controller:
        # Point the player at the mouse
        rotation = global_position.angle_to_point(mouse_pos) - 3.14/2
    else:
        # Or spin towards controller right axis direction

        # This will be true if there's no changes these axes
        if controller_vec2.length() != 0:
            # Do you enjoy magic numbers?
            rotation = controller_vec2.angle() + 3.14/2
    last_mouse_pos = local_mouse_pos

master func get_input():
        move_x = Input.get_action_strength('right') - Input.get_action_strength('left')
        # Up is negative???
        move_y = Input.get_action_strength('down') - Input.get_action_strength('up')
        fire_rate_multiplier = Input.get_action_strength('attack')

master func handle_dash(to_move):
    if Input.is_action_pressed('dash') and can_dash():
        # disable physics
        mode = MODE_KINEMATIC
        collision_layer = WALLS
        collision_mask = WALLS
        state = State.DASHING
        dash_from = position
        dash_to = position + to_move * DASH_DISTANCE
        dash_offset = Vector2(0.0, 0.0)
        $DashAnimation.play("Dash")
        $DashTimer.start()

master func handle_bash():
    if Input.is_action_just_pressed('bash') and can_bash():
        $BashPlayer.play()
        var facing = global_transform.basis_xform(Vector2(0, -1))
        print(facing)
        for enemy in get_tree().get_nodes_in_group("enemies"):
            var to_enemy = enemy.global_position - global_position
            var angle = acos(facing.dot(to_enemy.normalized()))
            if to_enemy.length_squared() < BASH_RANGE * BASH_RANGE and angle < BASH_ANGLE:
                enemy.apply_central_impulse(to_enemy.normalized() * BASH_IMPULSE)
        $BashTimer.start()

func can_fire():
    return $ShootTimer.is_stopped()

func can_dash():
    return $DashTimer.is_stopped()

func can_bash():
    return $BashTimer.is_stopped()

func clear_inputs():
    # Reset inputs
    # This makes it so the player doesn't keep moving
    # after they've released the movement key
    move_x = 0
    move_y = 0
    fire_rate_multiplier = 0.0

func stop_dash(_anim):
    state = State.NORMAL
    # re-enable physics
    mode = MODE_RIGID
    collision_layer = normal_layers 
    collision_mask = 1
    linear_velocity = Vector2(0.0, 0.0)

func proj(u, v):
    return u * u.dot(v) / u.dot(u)

func set_dash_position(alpha):
    # simulate move_and_slide
    var result = Physics2DTestMotionResult.new()
    var target = dash_from * (1.0 - alpha) + dash_to * alpha + dash_offset
    var motion = target - position
    if test_motion(motion, false, 0.08, result):
        # project motion onto the normal
        position += result.motion
        var offset = proj(result.collision_normal, result.motion_remainder)
        dash_offset -= offset
        motion = result.motion_remainder - offset
        if test_motion(motion, false, 0.08, result):
            position += result.motion
            dash_offset -= result.motion_remainder
            return
    position += motion

func damage():
    # don't take damage while dashing
    if state != State.DASHING:
        health_meater -= 4
        on_hurt(4)
        update_health()

func on_hurt(damage):
    get_node("/root/World/Overlay").on_hurt(damage)

func update_health():
    health_meater = min(health_meater, 100)
    if !is_multiplayer or is_network_master():
        get_node("/root/World/HUD/HUD").set_health(health_meater)
        if health_meater > 15 and low_meat:
            low_meat = false
            # fade out
            $FadeTween.remove_all()
            var current = $LowMeatPlayer.volume_db
            $FadeTween.interpolate_property($LowMeatPlayer, "volume_db", current, -80, 2, Tween.TRANS_QUAD, Tween.EASE_IN)
            $FadeTween.start()
        elif health_meater <= 15 and not low_meat:
            low_meat = true
            # fade in
            $FadeTween.remove_all()
            var current = $LowMeatPlayer.volume_db
            $FadeTween.interpolate_property($LowMeatPlayer, "volume_db", current, 3, 0.2, Tween.TRANS_QUAD, Tween.EASE_OUT)
            $FadeTween.start()

func _physics_process(_delta):
    if (is_multiplayer && !is_network_master()):
        return

    for object in $EatsRange.get_overlapping_bodies():
        if object.has_method("consume"):
            var how_much_eats = object.consume()
            if how_much_eats > 0:
                health_meater += how_much_eats
                var eats = AudioStreamPlayer.new()
                eats.stream = eats_stream
                eats.volume_db = 10.0
                eats.connect("finished", eats, "queue_free")
                eats.play()
                add_child(eats)
                get_node("/root/World/HUD/HUD").add_to_score(how_much_eats)
                update_health()

    if state == State.DASHING:
        set_dash_position(dash_alpha)
        return

    for object in get_colliding_bodies():
        if object.has_method("hurt_on_contact"):
            var damage = object.hurt_on_contact()
            health_meater -= damage
            on_hurt(damage)
            update_health()


    clear_inputs() 
    # Either do the rpc or normal versions of these calls
    if is_multiplayer:
        rpc("get_input")
        rpc("handle_rotation")
    else:
        get_input()
        handle_rotation()

    var to_move = Vector2(move_x,move_y)
    if to_move.length_squared() > 1.0:
        # Normalize vector so you can't move faster diagonally
        to_move = to_move.normalized()
    # And finally actually perform the movement
    linear_velocity = to_move * speed

    if fire_rate_multiplier > 0 && can_fire():
        var shoots = AudioStreamPlayer.new()
        shoots.stream = shoots_stream
        shoots.connect("finished", shoots, "queue_free")
        shoots.play()
        add_child(shoots)
        if is_multiplayer:
            rpc("fire")
        else:
            fire()

    if is_multiplayer:
        rpc("handle_dash", to_move)
    else:
        handle_dash(to_move)

    if is_multiplayer:
        rpc("handle_bash")
    else:
        handle_bash()

    if health_meater <= 0:
        save_score()
        if is_multiplayer and is_network_master():
            var players = get_node("/root/World/players")
            if players.get_child_count() <= 1:
                # If we're the last player shutdown the lobby
                var lobby = get_node("/root/Lobby")
                lobby.rpc("shutdown")
            else:
                # Or turn our player into a spectator
                add_spectator()
                rpc("remove_player")

        else:
            Globals.unload_world()
            get_tree().change_scene('res://scenes/Menu.tscn')
            get_node("/root/Globals")
    # print(health_meater)

func save_score():
	var score = get_node("/root/World/HUD/HUD").get_score()
	var file = File.new()
	file.open("user://score", File.WRITE)
	file.store_32(score)
	file.close()
	if !file.file_exists("user://high_score"):
		file.open("user://high_score", File.WRITE)
		file.store_32(score)
	else:
		file.open("user://high_score", File.READ)
		var high_score = file.get_32()
		file.close()
		if score > high_score:
			file.open("user://high_score", File.WRITE)
			file.store_32(score)
	file.close()

func add_spectator():
    var players = get_node("/root/World/players")
    var spectators = get_node("/root/World/spectators")
    var spectator = preload("res://scenes/Spectator.tscn").instance()
    # Add a spectator for the current game
    spectator.name = self.name
    spectator.set_network_master(self.get_network_master())
    spectator.position = self.position
    spectators.add_child(spectator)

remotesync func remove_player():
    var players = get_node("/root/World/players")

    # Remove the player
    players.remove_child(self)
    self.queue_free()

remotesync func fire():
    var new_bit = bacon_bits.instance()
    new_bit.position = position
    new_bit.rotation = rotation
    new_bit.add_collision_exception_with(self)
    get_parent().add_child(new_bit)
    $ShootTimer.start()
    health_meater -= 1
    update_health()
    can_fire = !can_fire

slave func update_trans(t):
        saved_trans = t

func _integrate_forces(state):
    if is_multiplayer:
        if (is_network_master()):
                rpc_unreliable("update_trans",state.transform)
        else:
                if (saved_trans != null):
                        state.transform = saved_trans
