extends Node

# This file contains global constants
const MAP_PATH = "/root/World/map/"

const TO_UNLOAD = ["World", "Lobby"] 

func unload_world():
    for node in TO_UNLOAD:
        if get_node("/root").has_node(node):
            get_node("/root/" + node).queue_free()

