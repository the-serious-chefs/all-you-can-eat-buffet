extends Node

const FIRST_WAIT = 6
const WAIT = 30
var wave_size = 10

var amount_spawned = 0

const ENEMIES = [preload("res://scenes/Sigmund.tscn"), \
                 preload("res://scenes/Octopus.tscn"), \
                 preload("res://scenes/FireWizard.tscn")]

func _ready():
    $NextWave.connect("timeout", self, "spawn_wave")
    $NextWave.wait_time = FIRST_WAIT
    $NextWave.start()

func spawn_wave():
    $Laugh.play()
    $NextWave.wait_time = WAIT
    $NextWave.start()
    var spawners = get_tree().get_nodes_in_group("spawners")
    for i in range(0, wave_size):
        if (get_tree().has_network_peer()):
            if (is_network_master()):
                var pick_spawner = randi() % spawners.size()
                var next_name = "Enemy" + str(amount_spawned)
                var next_enemy = spawners[pick_spawner].choose_enemy()
                spawners[randi() % spawners.size()].rpc("_spawn_enemy",next_name,next_enemy)
        else:
            spawners[randi() % spawners.size()].spawn_random()
        amount_spawned += 1
    wave_size = wave_size + floor(wave_size / 8) + 3
