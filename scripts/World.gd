extends Node2D

const levels = [ \
                 preload("res://maps/blighted_bridge.tscn"), \
                 preload("res://maps/crazy_corridor.tscn"), \
                 preload("res://maps/dreadful_dungeon.tscn"), \
                 preload("res://maps/horrific_hallway.tscn"), \
                 ]

# FOR TESTING PURPOSES
# const levels = [ preload("res://maps/dreadful_dungeon.tscn") ]

func _ready():
    randomize()
    # Check if we're in singleplayer
    if not get_tree().has_network_peer():
        new_random_map()
        var player = preload("res://scenes/Player.tscn").instance()
        get_node("players").add_child(player)

# This function checks if the map has a node called
# player_spawn, and if it does we move our player node there
func maybe_set_spawn():
    var spawn_path = "map/Level/player_spawn"
    if has_node(spawn_path):
        var player_spawn = get_node(spawn_path)
        $players.position = player_spawn.position

func instance_map(index):
    $map.add_child(levels[index].instance())
    maybe_set_spawn()

# Returns a random map index
func get_random_map():
    return randi() % levels.size()

func new_random_map():
    var index = get_random_map()
    instance_map(index)

func unload_map():
    for child in $map.get_children():
        $map.remove_child(child)
        child.queue_free()
