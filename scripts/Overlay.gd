extends CanvasLayer

const DECAY = 3

func _process(delta):
    $Hurt.modulate.a *= exp(-DECAY * delta)

func on_hurt(damage):
    $Hurt.modulate.a = min($Hurt.modulate.a + damage * 0.2, 0.6)
