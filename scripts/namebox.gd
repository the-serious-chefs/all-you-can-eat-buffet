extends TextEdit

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var default_text = "ENTER NAME"
export var file = "user://username"

# Called when the node enters the scene tree for the first time.
func _ready():
        var f = File.new()
        f.open(file, File.READ)
        var t = f.get_line()
        f.close()

        if t.empty():
                text = default_text
                # Write the default text into the file
                f.open(file,File.WRITE)
                f.store_string(text)
                f.close()

        else:
                text = t

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#       pass



func _on_TextEdit_text_changed():
        var f = File.new()
        f.open(file,File.WRITE)
        f.store_string(self.text)
