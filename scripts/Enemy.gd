extends RigidBody2D

export var speed: float
export var close_speed: float
export var close_distance: float = 128.0
export var stop_distance: float = 100.0
export var separation: float = 16.0
export var wall_avoidance: float = 8.0
export var blood_scene: PackedScene
export var health: int = 2
export var staggered_health: int = 1
export var contact_damage: int = 1
export var consumption_bonus: int = 5
export var idle_sound_min: float = 0.0
export var idle_sound_max: float = 1.0
export var projectile: PackedScene
export var projectile_speed: float = 400
export var track_projectiles: bool = false

const PLAYER_SEPARATION = 64.0

export var staggered_sprite: Texture

var path = null
var saved_trans = null
# yucky hardcoded nonsense
const WALLS = 2 

func path_length(path):
    var length = 0.0
    for i in range(0, path.size() - 1):
        length += path[i].distance_to(path[i + 1])
    return length

func close_to_player():
    for player in get_tree().get_nodes_in_group("players"):
        if global_position.distance_squared_to(player.global_position) < close_distance * close_distance:
            return true
    return false

func can_see_player():
    var min_distance = null
    var seen = null
    var space_state = get_world_2d().direct_space_state
    for player in get_tree().get_nodes_in_group("players"):
        # magic layer numbers
        var result = space_state.intersect_ray(global_position, player.global_position, [player], WALLS)
        if result.empty():
            var distance = player.global_position.distance_to(global_position)
            if min_distance == null or distance < min_distance:
                min_distance = distance
                seen = player
    return seen

func get_new_path():
    var min_length = null
    var space_state = get_world_2d().direct_space_state

    var map = get_node("/root/World/map/Level/Navmesh")

    for player in get_tree().get_nodes_in_group("players"):
        var to_player = map.get_simple_path(global_position, player.global_position, true)
        var length = path_length(to_player)
        if min_length == null or length < min_length:
            min_length = length
            path = to_player


    # convert to generic array for pop_front
    path = Array(path)
    # reset the timer again 0.3 seconds per path node
    $NewPathTimer.wait_time = 0.02 + min(0, path.size() - 2)  * 0.05
    $NewPathTimer.start()

func _ready():
    add_to_group("enemies")
    $NewPathTimer.connect("timeout", self, "get_new_path")
    $IdleSoundTimer.connect("timeout", self, "play_idle")
    play_idle()
    get_new_path()

func play_idle():
    if $IdleSound != null:
        $IdleSound.play()
    $IdleSoundTimer.wait_time = idle_sound_min + (idle_sound_max - idle_sound_min) * randf()
    $IdleSoundTimer.start()

# func _draw():
#     if path != null:
#         var color = Color(1.0, 0.0, 0.0)
#         var m = global_transform
#         for i in range(0, path.size() - 1):
#             draw_line(m.xform_inv(path[i]), m.xform_inv(path[i+1]), color, 2.0, true)

func limit_to(v, m):
    if v.length_squared() > m * m:
        return v.normalized() * m
    else:
        return v

func _physics_process(delta):
    # update()

    if (saved_trans != null && !is_network_master()):
            return
    if health <= staggered_health:
        # linear_velocity = Vector2(0, 0)
        return

    var steer = Vector2(0.0, 0.0)

    if projectile != null and $ProjectileTimer.is_stopped():
        print("trying to shoot")
        var target = can_see_player()
        if target != null:
            var p = projectile.instance()
            p.speed = projectile_speed
            p.position = position
            var shoot_at = target.global_position
            if track_projectiles:
                # predict movement (sort of)
                var time_to_hit = global_position.distance_to(target.global_position) / projectile_speed
                shoot_at += target.linear_velocity * time_to_hit
            p.rotation = (shoot_at - global_position).angle() + 3.14 / 2
            get_parent().add_child(p)
            $ProjectileTimer.start()

    var cur_speed = speed
    if close_to_player():
        cur_speed = close_speed

    # target towards the next point
    var next_point = path[1]
    var to_next = next_point - global_position
    # remove points that are passed
    while path.size() > 2 and to_next.length_squared() < 32 * 32:
        # print("pop")
        path.pop_front()
        next_point = path[1]
        to_next = next_point - global_position
    steer += (next_point - global_position).normalized() * cur_speed

    global_rotation = to_next.angle()

    # shape cast to check for nearby objects to avoid collision
    var avoidance_count = 0
    var steer_avoidance = Vector2(0.0, 0.0)
    for body in $Avoid.get_overlapping_bodies():
        var to_body = global_position - body.global_position
        var dist_sq = to_body.length_squared()
        if dist_sq > 0.0 and dist_sq < separation * separation:
            var factor = separation / sqrt(dist_sq)
            steer_avoidance += to_body.normalized() * factor
            avoidance_count += 1.0
    if avoidance_count > 0:
        steer_avoidance /= avoidance_count
        steer += 4.0 * steer_avoidance * cur_speed

    apply_central_impulse(delta * mass * 10 * limit_to(steer, cur_speed))

slave func update_trans(t):
    saved_trans = t

func _integrate_forces(state):
    if (get_tree().has_network_peer() && is_network_master()):
        rpc_unreliable("update_trans",state.transform)
    else:
        if (saved_trans != null):
            state.transform = saved_trans

puppet func sync_health(h):
    health = h
    check_stagger()

func damage():
    health -= 1
    if is_network_master():
        rpc("sync_health",health)
    check_stagger()

func check_stagger():
    if health <= staggered_health:
        $Sprite.set_texture(staggered_sprite)
    if health <= 0:
        # Remove the enemy from all clients
        if (get_tree().has_network_peer()):
            rpc("remove_enemy")
        # Or if we're in singleplayer do the non-networked version
        else:
            get_node("/root/World/HUD/HUD").add_to_score(consumption_bonus)
            remove_enemy()

func is_staggered():
    return (health <= staggered_health)

func bleed():
    var blood = blood_scene.instance()
    blood.transform = self.transform
    get_parent().add_child(blood)

remotesync func remove_enemy():
    bleed()
    queue_free()

func can_deal():
	return $AttackCooldown.is_stopped()

func hurt_on_contact():
    if can_deal():
        $AttackCooldown.start()
        return contact_damage
    else:
        return 0

func consume():
    if health <= staggered_health:
        if get_tree().has_network_peer():
            rpc("remove_enemy")
        else:
            remove_enemy()
        return consumption_bonus
    else:
        return 0
