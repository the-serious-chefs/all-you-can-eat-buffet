extends KinematicBody2D

export var wall_hit_sound: AudioStream
export var enemy_hit_sound: AudioStream

const speed = 1200
const KNOCKBACK = 1000

func _ready():
    $Sprite.frame = randi() % $Sprite.hframes

func _physics_process(delta):
    var facing = Vector2(sin(rotation), -cos(rotation))
    var collision = move_and_collide(delta * facing * speed, false)
    if collision != null:
        var player = AudioStreamPlayer.new()
        player.volume_db = 5
        if collision.collider.has_method("damage"):
            player.stream = enemy_hit_sound
            collision.collider.damage()
            # knockback
            var offset = global_position - collision.collider.global_position
            var impulse = facing * KNOCKBACK
            collision.collider.apply_impulse(offset, impulse)
        else:
            player.stream = wall_hit_sound
        player.connect("finished", player, "queue_free")
        get_parent().add_child(player)
        player.play()
        queue_free()
