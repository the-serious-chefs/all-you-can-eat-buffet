<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="Waylon's Spaceship" tilewidth="32" tileheight="32" tilecount="32" columns="8">
 <image source="waylons_set.png" width="256" height="128"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="3" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="2" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index">
   <object id="5" x="0" y="1" width="33" height="31"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index">
   <object id="2" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="13">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 32,32 0,0"/>
   </object>
   <object id="6" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index">
   <object id="1" x="0" y="32">
    <polygon points="0,0 32,-32 32,0 0,0"/>
   </object>
   <object id="2" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="22">
  <objectgroup draworder="index">
   <object id="1" x="0" y="32">
    <polygon points="0,0 32,-32 0,-32"/>
   </object>
   <object id="2" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="23">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32,0 32,32 0,0"/>
   </object>
   <object id="2" type="navigation" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
</tileset>
